# javascript kata starter

Use this as a starter template for any lightweight kata.

Uses es6 modules throughout


- Run tests in the browser (no installs needed): `npx live-server`
- Run tests in a terminal. Requires installation of a few dependencies
   - `npm init -y`
   - `npm install`
   - to test run `npm test`
