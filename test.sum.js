
var expect = chai.expect;

import sum from './sum.mjs';

describe('sum', function() {

    it('should return the sum of two args', function() {
      expect(sum('a', 'b')).to.equal('ab');
    });

    it('should return empty string when no arguments', function() {
      expect(sum()).to.equal('');
    });

});
